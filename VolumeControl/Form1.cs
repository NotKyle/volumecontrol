﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.IO.Ports;
using System.Diagnostics;
using System.Runtime.InteropServices;
using VideoPlayerController;

namespace VolumeControl
{
    public partial class Form1 : Form
    {
        SerialPort port = new SerialPort("COM5", 9600, Parity.None, 8, StopBits.One);
        int volumeValue = 0;
        int oldValue = 0;
        int selectedProgram;

        public Form1()
        {
            InitializeComponent();
            Debug.WriteLine(AudioManager.GetMasterVolume());
            var allProcesses = Process.GetProcesses();

            foreach (Process process in allProcesses)
                if (VolumeMixer.GetApplicationVolume(process.Id) != null)
                    listBox1.Items.Add(process.ProcessName + " # " + process.MainWindowTitle + " # " + process.Id);


            port.ReadTimeout = 2000;
            port.Open();
            port.DtrEnable = true; port.DataReceived += new SerialDataReceivedEventHandler(port_DataReceived);
        }

        private void port_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            int intBuffer;
            intBuffer = port.BytesToRead;
            byte[] byteBuffer = new byte[intBuffer];
            port.Read(byteBuffer, 0, intBuffer);
            if(port.IsOpen)
            {
               this.Invoke(new EventHandler(DoUpdate));
            }
        }

        private void DoUpdate(object sender, EventArgs e)
        {
            int num = 0;
            if(int.TryParse(port.ReadLine(), out num))
            volumeValue = num;
            
            if(volumeValue != oldValue)
            {
                oldValue = volumeValue;
                label2.Text = volumeValue + "%";

                if (selectedProgram != 0)
                {
                    VolumeMixer.SetApplicationVolume(selectedProgram, (float)volumeValue);
                }
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            port.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string[] splitProcess = listBox1.SelectedItem.ToString().Split('#');

            int processId = int.Parse(splitProcess[2]);

            Debug.WriteLine(processId);

            selectedProgram = processId;

            // int processId = int.Parse(splitProcess[2]);
        }
    }
}
